package com.example.iconicrazum.controller;

import com.example.iconicrazum.model.CategoryModel;
import com.example.iconicrazum.model.ProductModel;
import com.example.iconicrazum.model.TypeModel;
import com.example.iconicrazum.model.UsersModel;
import com.example.iconicrazum.repo.CategoryRepo;
import com.example.iconicrazum.repo.ProductRepo;
import com.example.iconicrazum.repo.TypeRepo;
import com.example.iconicrazum.repo.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/Manage")
public class MProductController {
    private final ProductRepo _ProductRepo;
    private final CategoryRepo _CategoryRepo;
    private final UsersRepo _UsersRepo;
    private final TypeRepo _TypeRepo;

    @Autowired
    public MProductController(ProductRepo _ProductRepo, CategoryRepo _CategoryRepo, UsersRepo _UsersRepo,TypeRepo _TypeRepo) {
        this._ProductRepo = _ProductRepo;
        this._CategoryRepo = _CategoryRepo;
        this._UsersRepo = _UsersRepo;
        this._TypeRepo = _TypeRepo;
    }

    @GetMapping("/HomeManage")
    public String NameUser(Model model) {
        Iterable<UsersModel> users = _UsersRepo.findAll();
        model.addAttribute("users", users);
        return "Manage/HomeManage";
    }

    // Действия с товарами

    @GetMapping("/MShowProducts")
    public String listProducts(Model model) {
        Iterable<ProductModel> products = _ProductRepo.findAll();
        model.addAttribute("products", products);
        return "Manage/MShowProducts";
    }

    @GetMapping("/MAddProducts")
    public String showAddProductsForm(Model model) {
        ProductModel product = new ProductModel();
        model.addAttribute("categories", _CategoryRepo.findAll());
        model.addAttribute("types", _TypeRepo.findAll());
        model.addAttribute("product", product);
        return "Manage/MAddProducts";
    }

    @PostMapping("/MAddProducts")
    public String addProducts(@Valid @ModelAttribute("product") ProductModel product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Manage/MAddProducts";
        }
        _ProductRepo.save(product);
        return "redirect:/Manage/MShowProducts";
    }

    @GetMapping("/GetTypesByCategory/{categoryId}")
    public ResponseEntity<List<TypeModel>> getTypesByCategory(@PathVariable Long categoryId) {
        Optional<CategoryModel> category = _CategoryRepo.findById(categoryId);

        if (category.isPresent()) {
            List<TypeModel> types = category.get().getProductTypes();
            return new ResponseEntity<>(types, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/MEditProducts/{id}")
    public String showEditProductsForm(@PathVariable("id") Long id, Model model) {
        ProductModel product = _ProductRepo.findById(id).orElse(null);
        if (product == null) {
            return "redirect:/Manage/MShowProducts";
        }
        model.addAttribute("product", product);
        return "Manage/MEditProducts";
    }

    @PostMapping("/MEditProducts/{id}")
    public String editProducts(@PathVariable("id") Long id, @Valid @ModelAttribute("product") ProductModel product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Manage/MEditProducts";
        }
        product.setId(id);
        _ProductRepo.save(product);
        return "redirect:/Manage/MShowProducts";
    }

    @GetMapping("/MDeleteProducts/{id}")
    public String deleteProducts(@PathVariable("id") Long id) {
        _ProductRepo.deleteById(id);
        return "redirect:/Manage/MShowProducts";
    }

    // Действия с категориями

    @GetMapping("/MAddCategories")
    public String listCategories(Model model) {
        Iterable<CategoryModel> categories = _CategoryRepo.findAll();
        model.addAttribute("categories", categories);
        CategoryModel category = new CategoryModel();
        model.addAttribute("category", category);
        return "Manage/MAddCategories";
    }

    @PostMapping("/MAddCategories")
    public String addCategories(@Valid @ModelAttribute("category") CategoryModel category, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Manage/MAddCategories";
        }
        _CategoryRepo.save(category);
        return "redirect:/Manage/MAddCategories";
    }

    @GetMapping("/MEditCategories/{id}")
    public String showEditCategoriesForm(@PathVariable("id") Long id, Model model) {
        CategoryModel category = _CategoryRepo.findById(id).orElse(null);
        if (category == null) {
            return "redirect:/Manage/MAddCategories";
        }
        model.addAttribute("category", category);
        return "Manage/MEditCategories";
    }

    @PostMapping("/MEditCategories/{id}")
    public String editCategories(@PathVariable("id") Long id, @Valid @ModelAttribute("category") CategoryModel category, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Manage/MEditCategories";
        }
        category.setId(id);
        _CategoryRepo.save(category);
        return "redirect:/Manage/MAddCategories";
    }

    @GetMapping("/MDeleteCategories/{id}")
    public ModelAndView deleteCategories(@PathVariable("id") Long id, Model model) {
        try {
            _CategoryRepo.deleteById(id);
            return new ModelAndView("redirect:/Manage/MAddCategories");
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Невозможно удалить категорию: некоторые товары относятся к данной категории");
            // Возвращаем ту же страницу
            return new ModelAndView("Manage/MAddCategories");
        }
    }

    // тип товаров

    @GetMapping("/MAddType")
    public String listTypes(Model model) {
        Iterable<CategoryModel> categories = _CategoryRepo.findAll();
        model.addAttribute("categories", categories);
        Iterable<TypeModel> types = _TypeRepo.findAll();
        model.addAttribute("types", types);
        TypeModel type = new TypeModel();
        model.addAttribute("type", type);
        return "Manage/MAddType";
    }
    @PostMapping("/MAddType")
    public String addTypes(@Valid @ModelAttribute("type") TypeModel type, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Manage/MAddType";
        }
        _TypeRepo.save(type);
        return "redirect:/Manage/MAddType";
    }
    @GetMapping("/MEditType/{id}")
    public String showEditTypesForm(@PathVariable("id") Long id, Model model) {
        TypeModel type = _TypeRepo.findById(id).orElse(null);
        if (type == null) {
            return "redirect:/Manage/MAddType";
        }
        model.addAttribute("type", type);
        Iterable<CategoryModel> categories = _CategoryRepo.findAll();
        model.addAttribute("categories", categories);
        return "Manage/MEditType";
    }
    @PostMapping("/MEditType/{id}")
    public String editTypes(@PathVariable("id") Long id, @Valid @ModelAttribute("type") TypeModel type, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Manage/MEditType";
        }
        type.setId(id);
        _TypeRepo.save(type);
        return "redirect:/Manage/MAddType";
    }
    @GetMapping("/MDeleteType/{id}")
    public String deleteTypes(@PathVariable("id") Long id) {
        _TypeRepo.deleteById(id);
        return "redirect:/Manage/MAddType";
    }
    @GetMapping("/Statistics")
    public String showGenderDistribution(Model model) {
        // Получите данные из базы данных (это просто пример, реализуйте свою логику)
        List<UsersModel> users = (List<UsersModel>) _UsersRepo.findAll();

        // Подсчет количества мужчин и женщин
        long maleCount = users.stream().filter(user -> "male".equalsIgnoreCase(user.getGender())).count();
        long femaleCount = users.stream().filter(user -> "female".equalsIgnoreCase(user.getGender())).count();

        // Создайте карту данных для передачи в Thymeleaf
        Map<String, Long> genderData = new HashMap<>();
        genderData.put("Мужчины", maleCount);
        genderData.put("Женщины", femaleCount);

        // Передайте данные в модель
        model.addAttribute("genderData", genderData);

        return "Manage/Statistics";
    }
}